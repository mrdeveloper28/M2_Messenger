







module.exports = function() {
    return function(hook) {
      // The user id
      const { _id } = hook.data;
      
      const user = hook.app.service('users');

      user.find({_id:_id}).then( result => {
            if(result.role !== 'admin')
            {
                reject();
            }
      })

      .catch( err => { throw err } )
  
      // Hooks can either return nothing or a promise
      // that resolves with the `hook` object for asynchronous operations
      return Promise.resolve(hook);
    };
  };