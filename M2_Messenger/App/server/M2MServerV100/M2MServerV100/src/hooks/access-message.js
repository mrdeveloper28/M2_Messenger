// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function accessMessage (hook) {
    
    const user = hook.params.user;
    const data = hook.data

	hook.data = {

		$or: [{sender:user._id},{reciver:user._id}]
	};

    return Promise.resolve(hook);
  };
};
