'use strict';

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function() {
  return function(hook) {
    // The authenticated user
    const user = hook.params.user;
    const reciver = hook.data.reciver;
    // The actual message text
    const text = hook.data.text.trim()//triming
      // Messages can't be longer than 400 characters
      .substring(0, 400)
      // Do some basic HTML escaping
      .replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');

    if(text === ''){
      throw new Error('Message cant be empty!');
    }

    // Override the original data
    hook.data = {

      text,
      
      sender:user._id,
      reciver:reciver,
      
      status:null,
      
    };

    // Hooks can either return nothing or a promise
    // that resolves with the `hook` object for asynchronous operations
    return Promise.resolve(hook);
  };
};
