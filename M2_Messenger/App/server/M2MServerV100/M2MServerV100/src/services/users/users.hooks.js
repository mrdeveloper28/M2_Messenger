const { authenticate } = require('feathers-authentication').hooks;
const { when, discard, softDelete, keep, iff, isProvider } = require('feathers-hooks-common');
const { restrictToOwner } = require('feathers-authentication-hooks');

const { hashPassword } = require('feathers-authentication-local').hooks;

//const processUser = require('../../hooks/process-user');

const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: '_id',
    ownerField: '_id'
  })
];

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ ...restrict ],
    create: [ hashPassword(), iff(isProvider('external'),keep('email','password'))],
    update: [ ...restrict, hashPassword() , iff(isProvider('external'),keep('password'))],
    patch: [ ...restrict, hashPassword() , iff(isProvider('external'),keep('password'))],
    remove: [ ...restrict ]
  },

  after: {
    all: [
      when(
        hook => hook.params.provider,
        discard('password')
      )
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
