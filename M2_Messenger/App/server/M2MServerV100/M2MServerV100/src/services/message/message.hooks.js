const { authenticate } = require('feathers-authentication').hooks;
const commonHooks = require('feathers-hooks-common');
const { restrictToOwner, restrictToRoles } = require('feathers-authentication-hooks');

const processMessage = require('../../hooks/process-message');
const accessMessage = require('../../hooks/access-message');

const restrict_sender = [
  restrictToOwner({
    idField: '_id',
    ownerField: 'sender'
  })
];

const restrict_reciver = [
  restrictToOwner({
    idField: '_id',
    ownerField: 'reciver'
  })
];

module.exports = {
  before: {
    all: [ 
      authenticate('jwt'),
      commonHooks.when(
        hook => hook.method !== 'find',
        commonHooks.softDelete()),
    ],
    find: [accessMessage()],
    get: [accessMessage()],
    create: [processMessage()],
    update: [processMessage()],
    patch: [processMessage()],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
