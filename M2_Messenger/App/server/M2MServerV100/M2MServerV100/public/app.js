const socket = io();
const client = feathers();

// Create the Feathers application with a `socketio` connection
client.configure(feathers.socketio(socket));

// Get the service for our `messages` endpoint
const messages = client.service('messages');

// Configure authentication
client.configure(feathers.authentication({
  storage: window.localStorage
}));

/*if(Cooki.get('token')==undefined){
  window.location.replace("http://google.com");
}*/