﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Messenger.Classes
{
    public class Data
    {
        public String email { get; set; }
        public String text { get; set; }
        public String _id { get; set; }
        public String reciver { get; set; }
        public String sender { get; set; }
        public String createdAt { get; set; }
        public String updatedAt { get; set; }
    }
}
