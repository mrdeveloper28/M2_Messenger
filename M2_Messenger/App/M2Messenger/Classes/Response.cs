﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Messenger.Classes
{
    public class Response
    {
        public int code { get; set; }
        public String accessToken { get; set; }
        public String message { get; set; }
        public int total { set; get; }
        public String email { get; set; }
        public String _id { get; set; }
        public String createdAt { get; set; }
        public String updatedAt { get; set; }
        public String status { get; set; }
        public List<Data> data;
    }
}
