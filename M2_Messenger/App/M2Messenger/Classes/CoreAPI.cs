﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Diagnostics;

namespace M2Messenger.Classes
{
    public sealed class CoreAPI
    {
        private static readonly CoreAPI instance = new CoreAPI();
        private CoreAPI() { }

        public static CoreAPI Instance
        {
            get
            {
                return instance;
            }
        }




        private static String HostUrl = "http://localhost:3030/";

        private static HttpClient client = new HttpClient();

        private bool isLogined = false;

        private Response res = new Response();

        private String email;

        private String pass;



        private void setToken(String token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        private void unsetToken()
        {
            client.DefaultRequestHeaders.Authorization = null;
        }



        public String getEmail()
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            return email;
        }

        public String getPass()
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            return pass;
        }



        private async Task<string> PostDataAsync(Dictionary<string, string> values, String subUrl)
        {
            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync(HostUrl + subUrl, content);

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<HttpResponseMessage> PatchAsync(HttpClient hclient, Uri requestUri, HttpContent iContent)
        {
            var method = new HttpMethod("PATCH");

            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            // In case you want to set a timeout
            //CancellationToken cancellationToken = new CancellationTokenSource(60).Token;

            try
            {
                response = await hclient.SendAsync(request);
                // If you want to use the timeout you set
                //response = await client.SendRequestAsync(request).AsTask(cancellationToken);
            }
            catch (TaskCanceledException e)
            {
                Debug.WriteLine("ERROR: " + e.ToString());
            }

            return response;
        }

        public async Task<HttpResponseMessage> deleteAsync(HttpClient hclient, Uri requestUri, HttpContent iContent)
        {
            var method = new HttpMethod("DELETE");

            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            // In case you want to set a timeout
            //CancellationToken cancellationToken = new CancellationTokenSource(60).Token;

            try
            {
                response = await hclient.SendAsync(request);
                // If you want to use the timeout you set
                //response = await client.SendRequestAsync(request).AsTask(cancellationToken);
            }
            catch (TaskCanceledException e)
            {
                Debug.WriteLine("ERROR: " + e.ToString());
            }

            return response;
        }

        private async Task<string> PatchDataAsync(Dictionary<string, string> values, String subUrl)
        {
            var content = new FormUrlEncodedContent(values);

            var response = await PatchAsync(client,new Uri(HostUrl + subUrl), content);

            return await response.Content.ReadAsStringAsync();
        }

        private async Task<string> deleteDataAsync(Dictionary<string, string> values, String subUrl)
        {
            var content = new FormUrlEncodedContent(values);

            var response = await deleteAsync(client, new Uri(HostUrl + subUrl), content);

            return await response.Content.ReadAsStringAsync();
        }

        private async Task<string> GetDataAsync(String subUrl, String Query = "")
        {
            var response = await client.GetAsync(HostUrl + subUrl + "?" + Query);

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<Response> LoginAsync(String email, String pass)
        {
            var values = new Dictionary<string, string>
                {
                    { "email", email },
                    { "password", pass },
                    { "strategy","local"}
                };

            res.accessToken = "";

            String result = await PostDataAsync(values, "authentication/");

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.accessToken == "")
            {
                throw new Exception(res.message);
            }

            setToken(res.accessToken);
            isLogined = true;
            this.email = email;
            this.pass = pass;

            return res;
        }

        public async Task<Response> updatePassAsync(String id, String pass)
        {
            var values = new Dictionary<string, string>
                {
                    { "password", pass }
                };

            //res. = "";??

            String result = await PatchDataAsync(values, "users/"+id);

            res = JsonConvert.DeserializeObject<List<Response>>(result)[0];

            if (true)//res.?? == "")
            {
                //throw new Exception(res.message);
            }

            this.pass = pass;

            return res;
        }

        public async Task<Response> deleteAccountAsync(String id)
        {
            var values = new Dictionary<string, string>{};

            //res. = "";??

            String result = await deleteDataAsync(values, "users/" + id);

            res = JsonConvert.DeserializeObject<List<Response>>(result)[0];

            if (true)//res.?? == "")
            {
                //throw new Exception(res.message);
            }

            setToken("");

            return res;
        }

        public void Logout()
        {
            unsetToken();
            isLogined = false;
        }

        public async Task<Response> SignupAsync(String email, String pass)
        {
            var values = new Dictionary<string, string>
                {
                    { "email", email },
                    { "password", pass }
                };

            res.email = "";

            String result = await PostDataAsync(values, "users/");

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.email != email)
            {
                throw new Exception(res.message);
            }

            return res;
        }

        public async Task<Response> SearchAsync(String email)
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            res.data = null;

            String query = "email=" + email;

            String result = await GetDataAsync("users/", query);

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.data.Count == 0)
            {
                throw new Exception("notFoundException");
            }

            return res;
        }

        public async Task<Response> GetMessagesAsync(String id)
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            res.data = null;

            String query = "recciver=" + id;

            String result = await GetDataAsync("message/");//,query);

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.data.Count == 0)
            {
                throw new Exception("notFoundException");
            }

            return res;
        }

        public async Task<Response> GetIDAsync(String email)
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            res._id = "";

            String query = "email=" + email;

            String result = await GetDataAsync("users/", query);

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.data[0]._id == "")
            {
                throw new Exception("notFoundException");
            }

            return res;
        }

        public async Task<Response> GetEmailAsync(String id)
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            res.email = "";

            String query = "_id=" + id;

            String result = await GetDataAsync("users/", query);

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res.data[0].email == "")
            {
                throw new Exception("notFoundException");
            }

            return res;
        }

        public async Task<Response> SendMessageAsync(String reciver, String text)
        {
            if (!isLogined)
            {
                throw new Exception("UserNotLogined!");
            }

            res._id = "";

            var values = new Dictionary<string, string>
                {
                    { "text", text },
                    { "reciver", reciver }
                };

            String result = await PostDataAsync(values, "message/");

            res = JsonConvert.DeserializeObject<Response>(result);

            if (res._id == "")
            {
                throw new Exception("notFoundException");
            }

            return res;
        }
    }
}
