﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Messenger
{
    public class MrDeveloperMessageBoxRecive
    {
        public String Message { get; set; }
        public String Info { set; get; }
        public float scrollSize { get; set; }
    }

    public class MrDeveloperMessageBoxSend
    {
        public String Message { get; set; }
        public String Info { set; get; }
        public float scrollSize { get; set; }
    }
}
