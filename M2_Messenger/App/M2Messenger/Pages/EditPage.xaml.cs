﻿using M2Messenger.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace M2Messenger.Pages
{
    /// <summary>
    /// Interaction logic for EditPage.xaml
    /// </summary>
    public partial class EditPage : Page
    {
        public EditPage()
        {
            InitializeComponent();
        }

        private CoreAPI core = CoreAPI.Instance;

        private void BtnEditDeleteReq_Click(object sender, RoutedEventArgs e)
        {
            BtnEditEdit.Visibility = Visibility.Hidden;
            BtnEditDeleteReq.Visibility = Visibility.Hidden;
            TxtEditNewPass.Visibility = Visibility.Hidden;
            LblEditNewPass.Visibility = Visibility.Hidden;
            TxtEditNewPassconfirm.Visibility = Visibility.Hidden;
            LblEditNewPassconfirm.Visibility = Visibility.Hidden;
            BtnEditBacktoHome.Visibility = Visibility.Hidden;
            TxtEditLastPass.Visibility = Visibility.Hidden;
            LblEditLastPass.Visibility = Visibility.Hidden;

            BtnEditDelete.Visibility = Visibility.Visible;
            TxtEditPass.Visibility = Visibility.Visible;
            LblEditPass.Visibility = Visibility.Visible;
            TxtEditEmail.Visibility = Visibility.Visible;
            LblEditEmail.Visibility = Visibility.Visible;
            BtnEditBacktoEdit.Visibility = Visibility.Visible;

        }

        private void BtnEditBacktoHome_Click(object sender, RoutedEventArgs e)
        {
            NavigationService NavHome = NavigationService.GetNavigationService(this);
            NavHome.Navigate(new Uri("Pages/HomePage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void BtnEditBacktoEdit_Click(object sender, RoutedEventArgs e)
        {
            BtnEditEdit.Visibility = Visibility.Visible;
            BtnEditDeleteReq.Visibility = Visibility.Visible;
            TxtEditNewPass.Visibility = Visibility.Visible;
            LblEditNewPass.Visibility = Visibility.Visible;
            TxtEditNewPassconfirm.Visibility = Visibility.Visible;
            LblEditNewPassconfirm.Visibility = Visibility.Visible;
            BtnEditBacktoHome.Visibility = Visibility.Visible;
            TxtEditLastPass.Visibility = Visibility.Visible;
            LblEditLastPass.Visibility = Visibility.Visible;

            BtnEditDelete.Visibility = Visibility.Hidden;
            TxtEditPass.Visibility = Visibility.Hidden;
            LblEditPass.Visibility = Visibility.Hidden;
            TxtEditEmail.Visibility = Visibility.Hidden;
            LblEditEmail.Visibility = Visibility.Hidden;
            BtnEditBacktoEdit.Visibility = Visibility.Hidden;


        }

        private async void BtnEditEdit_Click(object sender, RoutedEventArgs e)
        {
            if (TxtEditLastPass.Text == core.getPass())
            {
                if (TxtEditNewPass.Text == TxtEditNewPassconfirm.Text)
                {
                    Response res = await core.updatePassAsync((await core.GetIDAsync(core.getEmail())).email, TxtEditNewPass.Text);

                    if (res.email != "")
                    {
                        MessageBox.Show("change your pass was successful.");

                        NavigationService NavHome = NavigationService.GetNavigationService(this);
                        NavHome.Navigate(new Uri("Pages/HomePage.xaml", UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show("Some thing went wrong!");
                    }
                }
                else
                {
                    MessageBox.Show("confirm error!");
                }
            }
            else
            {
                MessageBox.Show("previous pass error!");
            }
        }

        private async void BtnEditDelete_Click(object sender, RoutedEventArgs e)
        {
            if (TxtEditPass.Text == core.getPass())
            {

                Response res = await core.deleteAccountAsync((await core.GetIDAsync(core.getEmail())).email);

                if (res.email != "")
                {
                    MessageBox.Show("deleted account was successful.");

                    NavigationService NavHome = NavigationService.GetNavigationService(this);
                    NavHome.Navigate(new Uri("Pages/MainPage.xaml", UriKind.RelativeOrAbsolute));
                }
                else
                {
                    MessageBox.Show("Some thing went wrong!");
                }

            }
            else
            {
                MessageBox.Show("previous pass error!");
            }
        }

        private void BtnLogOut_Click(object sender, RoutedEventArgs e)
        {
            core.Logout();
            NavigationService NavHome = NavigationService.GetNavigationService(this);
            NavHome.Navigate(new Uri("Pages/MainPage.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
