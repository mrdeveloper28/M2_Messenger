﻿using System.Windows;
using System.Windows.Controls;
using System.Drawing;
using System.Windows.Navigation;
using System;
using System.Net.Mail;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using M2Messenger.Classes;

namespace M2Messenger
{
    /// <summary>
    /// Interaction logic for SignupPage.xaml
    /// </summary>
    public partial class SignupPage : Page
    {
        public SignupPage()
        {
            InitializeComponent();
        }

        private CoreAPI core = CoreAPI.Instance;

        private bool MailIsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private async void BtnRegRegister_Click(object sender, RoutedEventArgs e)
        {
            String email = TxtRegEmail.Text;
            String pass = TxtRegPass.Text;
            String confirm = TxtRegPassconfirm.Text;

            if (confirm != pass)
            {
                TxtRegPassconfirm.Text = "";
                MessageBox.Show("passwordd not confirmed!", "Error :", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (email!="" && MailIsValid(email))
            {
                BtnRegRegister.IsEnabled = false;
                TxtRegEmail.IsEnabled = false;
                TxtRegPass.IsEnabled = false;
                TxtRegPassconfirm.IsEnabled = false;

                try
                {
                    Response res = await core.SignupAsync(email, pass);


                    MessageBox.Show("\"" + email + "\" was registered successful.", "Info :", MessageBoxButton.OK, MessageBoxImage.Information);

                    NavigationService NavHome = NavigationService.GetNavigationService(this);
                    NavHome.Navigate(new Uri("Pages/MainPage.xaml", UriKind.RelativeOrAbsolute));
                }
                catch(Exception ex)
                {
                    MessageBox.Show("SomeThingWrong: " + ex.Message);

                    BtnRegRegister.IsEnabled = true;
                    TxtRegEmail.IsEnabled = true;
                    TxtRegPass.IsEnabled = true;
                    TxtRegPassconfirm.IsEnabled = true;
                    TxtRegEmail.Text = "";
                    TxtRegPass.Text = "";
                    TxtRegPassconfirm.Text = "";

                }
            }
            else
            {
                MessageBox.Show("please enter a valid email address", "Error :", MessageBoxButton.OK, MessageBoxImage.Error);
                TxtRegEmail.Text = "";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService NavMain = NavigationService.GetNavigationService(this);
            NavMain.Navigate(new Uri("Pages/MainPage.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}