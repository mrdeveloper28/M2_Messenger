﻿using M2Messenger.Classes;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Net.Mail;
using System.Threading.Tasks;

namespace M2Messenger
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private CoreAPI core = CoreAPI.Instance;

        private bool MailIsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private async void hpBtnLogin_Click(object sender, RoutedEventArgs e)
        {
            String email = hpTxtUsername.Text;
            String pass = hpTxtPassword.Text;

            if (MailIsValid(email))
            {
                hpBtnLogin.IsEnabled = false;
                hpBtnRegister.IsEnabled = false;
                hpTxtPassword.IsEnabled = false;
                hpTxtUsername.IsEnabled = false;

                try
                {
                    Response res = await core.LoginAsync(email, pass);

                    NavigationService NavHom = NavigationService.GetNavigationService(this);
                    NavHom.Navigate(new Uri("Pages/HomePage.xaml", UriKind.RelativeOrAbsolute));
                }
                catch(Exception ex)
                {
                    MessageBox.Show("SomeThingWrong: login failed!");

                    hpBtnLogin.IsEnabled = true;
                    hpBtnRegister.IsEnabled = true;
                    hpTxtPassword.IsEnabled = true;
                    hpTxtUsername.IsEnabled = true;

                }
            }
            else
            {
                MessageBox.Show("please enter a valid email address.", "Error :", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void hpBtnRegister_Click(object sender, RoutedEventArgs e)
        {
            NavigationService NavHom = NavigationService.GetNavigationService(this);
            NavHom.Navigate(new Uri("Pages/SignupPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void hpTxtUsername_GotFocus(object sender, RoutedEventArgs e)
        {
            if (hpTxtUsername.Text == "نام کاربری")
            {
                hpTxtUsername.Text = "";
            }
        }

        private void hpTxtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            if (hpTxtUsername.Text == "")
            {
                hpTxtUsername.Text = "نام کاربری";
            }
        }



        private void hpTxtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (hpTxtPassword.Text == "رمز عبور")
            {
                hpTxtPassword.Text = "";
            }
        }

        private void hpTxtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (hpTxtPassword.Text == "")
            {
                hpTxtPassword.Text = "رمز عبور";
            }
        }
    }


}
