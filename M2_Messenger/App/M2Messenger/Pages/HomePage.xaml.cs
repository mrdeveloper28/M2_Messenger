﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using M2Messenger.Classes;
using System.Timers;
using System.Windows.Threading;

namespace M2Messenger.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    ///

    public partial class HomePage : Page
    {
        //String email;
        String id;
        String reciver_id, reciver_email;
        public HomePage()
        {
            InitializeComponent();

            GetUser();
        }

        private async void GetUser()
        {
            reciver_email = core.getEmail();

            Response res = await core.GetIDAsync(reciver_email);
            reciver_id = res.data[0]._id;
            id = reciver_id;

            init();

            DispatcherTimer aTimer = new DispatcherTimer();
            aTimer.Tick += new EventHandler(OnTimedEvent);
            aTimer.Interval = new TimeSpan(0,0,2);
            aTimer.Start();
        }

        private void OnTimedEvent(object source, EventArgs e)
        {
            init();
        }

            private async void init()
        {
            txt_hp_username.Text = reciver_email;

            if (icMessageContainer != null)
            {
                icMessageContainer.Items.Clear();
            }

            if (contactList != null)
            {
                contactList.Items.Clear();
            }

            try
            {
                Response res = await core.GetMessagesAsync("");

                if (res.data.Count != 0)
                {
                    foreach (Data message in res.data)
                    {
                        if (message.reciver == id)
                        {
                            if (message.sender == reciver_id)
                            {
                                icMessageContainer.Items.Add(new MrDeveloperMessageBoxRecive() { Message = message.text, Info = message.updatedAt + ((res.status == null) ? "$" : "#") });
                            }
                        }

                        if (message.sender == id && message.reciver == reciver_id)
                        {
                            icMessageContainer.Items.Add(new MrDeveloperMessageBoxSend() { Message = message.text, Info = message.updatedAt + ((res.status == null) ? "$" : "#") });
                        }

                        if (message.sender == id || message.reciver == id)
                        {
                            if (contactList != null)
                            {
                                //contactList.Items.Clear();

                                bool found = false;

                                foreach (MrDeveloperContact c in contactList.Items)
                                {
                                    if (c.id == message.sender)
                                    {
                                        found = true;
                                    }
                                }

                                if (!found)
                                {
                                    Response r = await core.GetEmailAsync(message.sender);
                                    contactList.Items.Add(new MrDeveloperContact() { Name = r.data[0].email, id = r.data[0]._id });
                                }
                            }
                            else
                            {
                                Response r = await core.GetEmailAsync(message.sender);
                                contactList.Items.Add(new MrDeveloperContact() { Name = r.data[0].email, id = r.data[0]._id });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //nothing
            }
        }

        private CoreAPI core = CoreAPI.Instance;

        private async void BtnHomeSend_Click(object sender, RoutedEventArgs e)
        {
            String text = TxtHomeMessage.Text.Trim();

            if (text == "")
            {
                return;
            }

            Response res = await core.SendMessageAsync(reciver_id, text);

            icMessageContainer.Items.Add(new MrDeveloperMessageBoxSend() { Message = text, Info = res.updatedAt + ((res.status == null) ? "$" : "#") });
        }

        private void TxtHomeSend_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TxtHomeMessage.Text == ".متن خود را اینجا وارد کنید")
            {
                TxtHomeMessage.Text = " ";
            }
        }

        private void TxtHomeSearchField_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TxtHomeSearchField.Text == "ایمیل مخاطب خود را وارد کنید")
            {
                TxtHomeSearchField.Text = "";
            }
        }

        private void txt_hp_username_MouseDown(object sender, MouseButtonEventArgs e)
        {
        }
        private class MrDeveloperContact
        {
            public string Name { get; set; }
            public string id { get; set; }

        }

        private async void BtnHomeSearch_Click(object sender, RoutedEventArgs e)
        {

            String text = TxtHomeSearchField.Text.Trim();
            TxtHomeSearchField.IsEnabled = false;

            if (text == "")
            {
                init();
            }
            else
            {
                if (contactList != null)
                {
                    contactList.Items.Clear();
                }

                try
                {
                    Response res = await core.SearchAsync(text);

                    foreach (Data usre in res.data)
                    {
                        contactList.Items.Add(new MrDeveloperContact() { Name = usre.email, id = usre._id });
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("NotFound!");
                }
            }

            TxtHomeSearchField.IsEnabled = true;
        }

        private void TxtHomeSearchField_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService NavEdit = NavigationService.GetNavigationService(this);
            NavEdit.Navigate(new Uri("Pages/EditPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void contactList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            MrDeveloperContact selected = (MrDeveloperContact)e.AddedItems[0];
            if (selected != null)
            {
                reciver_email = selected.Name;
                reciver_id = selected.id;
                init();
            }

            //e.AddedItems.Clear();
        }
    }
}