M2 Messenger Scenario
==

Login Scenario
---
* **Goal:** This scenario explain that how user can log in
* **User:** A member of Vali Asr University.
* **Equipment:** A personal computer which is connected to our server.
* **Scenario:** 
1. User A open M2 Messenger app.
2. User A must connect app to local server.
3. After opening the app, user A must enter email and password.
4. If email and password both are correct, the user will Log in successfully.

Register Scenario
---
* **Goal:** This scenario explain that how user can register.
* **User:** A member of Vali Asr University.
* **Equipment:** A personal computer which is connected to our server.
* **Scenario:** 
1. User A open M2 Messenger app.
2. User A must connect app to local server.
3. After opening the app, user A must click on Register button and go to Register page.
4. Now user A must enter a valid email and a password.
5. After that user A must click on Register button.
6. If entered email is valid and user A hasn't an account on it in this app, the user will Register successfully.

Sending Message Scenario
---
* **Goal:** This scenario explain that how user can send message.
* **User:** A member of Vali Asr University.
* **Equipment:** A personal computer which is connected to our server.
* **Scenario:** 
1. User A open M2 Messenger app.
2. User A should connect app to local server.
3. After opening the app, user A should Log In.
4. If user A doesn't have account, he/she must Register and then Log In.
5. User A should search a contact to send message to it.
6. After searching the contact, user A should write his/her message at the message box.
7. Then user A must click Send Button to send his/her message. 



