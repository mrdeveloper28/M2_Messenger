Requirement
==
Functional
--
1. Register with email
2. Deleting Account
3. Log In
4. Log Out
5. Changing Password
6. Send Message to Other
7. Receive Message From Other
8. Searching others


Non-Functional
--
1. Windows Application
2. Able to Run on Windows 7, 8, 8.1 and 10
3. Good UX
4. Beautiful UI
5. Persian Language