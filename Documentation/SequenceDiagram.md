[Login Sequence Diagram] (https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Login%20Sequence%20Diagram.png)

[Register Sequence Diagram](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Register%20Sequence%20Diagram.jpg)

[Send Message Sequence Diagram](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Send%20Message%20Sequence%20Diagram.png)

[Change Password Sequence Diagram](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Change%20Password%20Sequence%20Diagram.png)

[Delete Account Sequence Diagram](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Delete%20Account%20Sequence%20Diagram.png)

[Searching Others Sequence Diagram](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/Pictures/Searching%20Others%20%20Sequence%20Diagram.png)