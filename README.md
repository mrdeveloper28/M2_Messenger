M2_Messenger Application
==
With this app you can create account and send message to your friends easily in an user friendly enviroment.



Features
--
* Create account easily
* Send message to your friends.
* Receive message from your friends.
* Pleasant user interface

Analyse and Design
--
* [Scenario](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/SENARIO.md)
* [Usecases](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/USECASE.md)
* [Requirements](https://gitlab.com/mrdeveloper28/M2_Messenger/blob/master/Documentation/REQUIREMENTS.md)

Phases of Project
--
1. Login
2. Register
3. Edit Account
4. Delete Account
5. Send Message
6. Receive Message
7. Searching Others

Developers
--

| ID            | Name |
| ---           |        ------- |
| @MrDeveloper28 | Mohammad Mozafarifard |
| @zahrarasti         | Zahra Rasti    |


